const express = require('express');
const Spotify = require('./server/spotify')
const path = require('path')

const app = express();
const port = process.env.PORT || 5000;

app.get('/api/auth', Spotify.auth);
app.get('/api/playlists', Spotify.getPlaylists);
app.get('/api/playlists/:id/tracks', Spotify.getPlaylistTracks);
app.get('/api/tracks/:id', Spotify.getTrackDetails);

// Serve client on production
if (process.env.NODE_ENV === 'production') {
    app.use(express.static(path.join(__dirname, 'client/build')));
    app.get('*', function(req, res) {
        res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
    });
}

app.listen(port, () => console.log(`Listening on port ${port}`));