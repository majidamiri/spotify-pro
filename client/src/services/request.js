/* eslint no-console: "off" */
import axios from 'axios';

export default function request(url, data, method = 'GET', headers) {
    let params = {
        url,
        method,
        data: method !== 'GET' ? data : undefined,
        params: method === 'GET' ? data : undefined,
        headers
    };
    return axios(url, params);
}
