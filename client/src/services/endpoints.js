export const AUTH_API = '/api/auth';
export const PLAYLISTS_API = '/api/playlists'
export const PLAYLIST_TRACKS_API = id => `/api/playlists/${id}/tracks`
export const TRACK_DETAILS_API = id => `/api/tracks/${id}`