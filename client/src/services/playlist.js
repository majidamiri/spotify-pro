import request from "./request";
import {PLAYLIST_TRACKS_API, PLAYLISTS_API} from "./endpoints";

export const getPlaylists = () => request(PLAYLISTS_API)
export const getPlaylistsTracks = id => request(PLAYLIST_TRACKS_API(id))