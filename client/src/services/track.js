import request from "./request";
import {TRACK_DETAILS_API} from "./endpoints";

export const getTrackDetails = id => request(TRACK_DETAILS_API(id))
