import request from "./request";
import {AUTH_API} from "./endpoints";

export const auth = () => {
    return request(AUTH_API)}