import React, {useEffect, useState} from 'react';
import {Typography, Card, CardContent, CardMedia} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {getTrackDetails} from "../../services/track";
import Loading from "../../components/Common/Loading";

const useStyles = makeStyles(theme => ({
    card: {
        display: 'flex',
        justifyContent: 'space-between',
        [theme.breakpoints.down('md')]: {
            flexDirection: 'column',
        },
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
        flex: '1 1 0px'
    },
    cover: {
        flex: '1 1 0px',
        width: '100%',
        height: '400px',
        [theme.breakpoints.down('md')]: {
            flex: '1 1 auto',
            height: '200px',
        },
    },
}));

function Track({match}) {
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState([])
    const classes = useStyles();
    useEffect(() => {
        function fetchPlaylists() {
            getTrackDetails(match.params.id)
                .then(res => setData(res.data))
                .catch(e => console.log('error while fetching playlist tracks', e))
                .finally(() => {
                    setLoading(false)
                })
        }
        fetchPlaylists()
    }, [match.params.id])
    if (loading)
        return <Loading show/>
    return (
        <Card className={classes.card}>
            <div className={classes.details}>
                <CardContent>
                    <Typography component="h5" variant="h5">
                        {data.name}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                        Artists: {data.artists.map(ar => `${ar.name}, `)}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                        Album: {data.album.name}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                        Release Date: {data.album.release_date}
                    </Typography>
                </CardContent>
            </div>
            <CardMedia
                className={classes.cover}
                image={data.album && data.album.images[0].url}
                title="Live from space album cover"
            />
        </Card>
    );
}

export default Track;