import React, {useEffect, useState} from 'react';
import {Typography} from "@material-ui/core";
import qs from "qs";
import idx from "idx";
import Card from "../../components/Card";
import {getPlaylistsTracks} from "../../services/playlist";
import Loading from "../../components/Common/Loading";
import GridList from "../../components/GridList"
import {msToMinutesAndSeconds} from "../../utils/helper";
import Routes from "../Router/Routes";

function Playlist({match, location}) {
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState([])
    useEffect(() => {
        function fetchPlaylists() {
            setLoading(true)
            getPlaylistsTracks(match.params.id)
                .then(res => setData(res.data.items))
                .catch(e => console.log('error while fetching playlist tracks', e))
                .finally(() => {
                    setLoading(false)
                })
        }
        fetchPlaylists()
    }, [match.params.id])

    const params = qs.parse(location.search.slice(1));
    const title = params.title || "";
    return (
        <>
            <Typography gutterBottom variant="h4">Playlist "{title}" Tracks</Typography>
            <Loading show={loading}/>
            <GridList
                items={data}
                renderItem={card => <Card
                    key={card.track.id}
                    title={card.track.name}
                    id={card.track.id}
                    image={idx(card, _ => _.track.album.images[0].url)}
                    link={`${Routes.Track}/${card.track.id}?title=${card.track.name}`}
                    renderDesc={() => (
                        <>
                            <Typography gutterBottom variant="h6" color="textSecondary">
                                {idx(card, _ => _.track.artists[0].name)}
                            </Typography>
                            <Typography gutterBottom variant="subtitle2" color="textSecondary">
                                Popularity: {card.track.popularity}, Duration: {msToMinutesAndSeconds(card.track.duration_ms)}
                            </Typography>
                        </>
                    )}
                    isTrack
                />}
            />
        </>
    );
}

export default Playlist;