import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Routes from './Routes.json'
import Main from "../Main";
import Playlist from "../Playlist";
import Track from "../Track";

const ROUTES = [
    {path: Routes.Main, cmp: Main, exact: true},
    {path: `${Routes.Playlist}/:id`, cmp: Playlist, exact: true},
    {path: `${Routes.Track}/:id`, cmp: Track, exact: true},
]

function Router() {
    return (
        <BrowserRouter>
            <Switch>
                {ROUTES.map((r, i) => <Route path={r.path} component={r.cmp} exact={r.exact} key={`route-${i}`}/>)}
            </Switch>
        </BrowserRouter>
    );
}

export default Router;