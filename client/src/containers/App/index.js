import React, {useEffect, useState} from 'react';
import {CssBaseline, Container} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import Router from "../Router";
import Header from "../../components/Layout/Header"
import Loading from "../../components/Common/Loading";
import {auth} from "../../services/auth";
import Axios from "axios";

const useStyles = makeStyles(theme => ({
    container: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    }
}));

export default function App() {
    const classes = useStyles();
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        auth()
            .then(res => {
                Axios.defaults.headers.common["Authorization"] = `Bearer ${res.data.token}`
            })
            .catch(e => console.log('error while fetching token', e))
            .finally(() => {
                setLoading(false)
            })
    }, [])

    if (loading)
        return <Loading pageLoading show/>
    return (
        <>
            <CssBaseline/>
            <Header/>
            <main>
                <Container className={classes.container} maxWidth="md">
                    <Router/>
                </Container>
            </main>
        </>
    );
}