import React, {useEffect, useState} from 'react';
import {Typography} from "@material-ui/core";
import Card from "../../components/Card";
import {getPlaylists} from "../../services/playlist";
import Loading from "../../components/Common/Loading";
import GridList from "../../components/GridList"
import Routes from "../../containers/Router/Routes.json"

function Main(props) {
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState([])
    useEffect(() => {
        fetchPlaylists()
    }, [])
    
    function fetchPlaylists() {
        setLoading(true)
        getPlaylists()
            .then(res => setData(res.data.playlists.items))
            .catch(e => console.log('error while fetching playlists', e))
            .finally(() => {
                setLoading(false)
            })
    }
    return (
        <>
            <Typography gutterBottom variant="h4">Playlists</Typography>
            <Loading show={loading}/>
            <GridList
                items={data}
                renderItem={card => <Card
                    key={card.id}
                    title={card.name}
                    id={card.id}
                    image={card.images[0].url}
                    link={`${Routes.Playlist}/${card.id}?title=${card.name}`}
                    renderDesc={() => (
                        <Typography gutterBottom variant="h6" color="textSecondary">
                            {card.tracks.total} tracks
                        </Typography>
                    )}
                />}
            />
        </>
    );
}

export default Main;