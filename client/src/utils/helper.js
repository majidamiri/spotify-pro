/**
 * Convert ms to minutes and second
 * @param  {Number} ms Time in miliseconds
 * @return {String} Time in minutes and seconds
**/

export function msToMinutesAndSeconds(ms) {
    const minutes = Math.floor(ms / 60000);
    const seconds = ((ms % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}