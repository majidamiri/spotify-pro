import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    pageLoading: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        width: '100vw',
        height: '100vh'
    },
}));

const Loading = (props) => {
    const classes = useStyles();
    const { show, pageLoading } = props;
    if(!show) return null;
    if (pageLoading)
        return (
            <div className={classes.pageLoading}>
                <CircularProgress size={60}/>
            </div>
        )
    return (
        <CircularProgress />
    );
}

export default Loading;
