import React from 'react';
import { Toolbar, Typography, AppBar } from "@material-ui/core";

export default function Header() {
    return (
        <AppBar position="relative">
            <Toolbar>
                <Typography variant="h6" color="inherit" noWrap>
                    Spotify
                </Typography>
            </Toolbar>
        </AppBar>
    );
}