import React from 'react';
import PropTypes from 'prop-types';
import {CardMedia, CardContent, Typography, CardActionArea, Grid, Card as MuiCard} from "@material-ui/core";
import {makeStyles} from '@material-ui/core/styles'
import styled from "styled-components";
import {Link as RouterLink} from "react-router-dom"

const useStyles = makeStyles(theme => ({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
}));

const Link = styled(RouterLink)`
  && {
    text-decoration: none;
    &:hover {
      text-decoration: none;
    }
  }
`;

function Card({title, image, isTrack, link, renderDesc}) {
    const classes = useStyles();
    return (
        <Grid item xs={12} sm={6} md={4}>
            <Link to={link}>
                <MuiCard className={classes.card}>
                    <CardActionArea>
                        {image && (
                            <CardMedia
                                className={classes.cardMedia}
                                image={image}
                                title={title}
                            />
                        )}
                        <CardContent className={classes.cardContent}>
                            <Typography gutterBottom={!isTrack} variant="h5" component="h2">
                                {title}
                            </Typography>
                            {renderDesc()}
                        </CardContent>
                    </CardActionArea>
                </MuiCard>
            </Link>
        </Grid>
    );
}

Card.propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    image: PropTypes.string,
    isTrack: PropTypes.bool,
    artist: PropTypes.string,
};

Card.defaultProps = {
    isTrack: false,
    image: ""
}

export default Card;