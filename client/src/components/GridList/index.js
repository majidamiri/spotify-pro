import React from 'react';
import PropTypes from 'prop-types';
import {Grid} from "@material-ui/core";

function GridList({ items, renderItem }) {
    return (
        <Grid container spacing={4}>
            {items.map(card => renderItem(card))}
        </Grid>
    );
}

GridList.propTypes = {
    items: PropTypes.array,
    renderItem: PropTypes.func,
};

export default GridList;