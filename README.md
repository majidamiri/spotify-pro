Running the Project
----
The project has main two parts.

- Client: This part is the react.js project that is based on `create-react-app` and runs on port `3000`. You can simply run it with `yarn client`.
- Server: Because of CORS issues with Spotify API I decided to run an instance of Express.js as backend and handle the requests through it. It runs on port `5000` and proxies when using with client. Run it with `yarn server` or `yarn start`.
- Dev: To run client and backend at the same time you can use `yarn dev`. It uses `concurrently` lib to make the run parallel.

Project Structure
----
The backend structure is as you considered, so I just mention some notes about the frontend part.

- `server.js`: It handles the requests to Spotify API and also serves the client in production mode.
- `config.js`: It just holds some information about Spotify API.
- `/server`: It has a `spotify.js` where I implemented Spotify requests logic.
- The frontend part is created based on `create-react-app` and all the files are included in `/client` directory.
- `Axios` lib is used for requests.
- I used `Material UI` and `Styled Components` for styling.
- `/components` dir includes the main components of the app.
- `/containers` dir includes the pages or main wrappers of the app.
- `/services` dir includes all the stuff that is related to requests to backend.
- `/utils` includes some useful functions and helpers that mostly handles some logic.
- There is nothing weird in the code. I just tried to make comments wherever possible.

More...
----
Test is a must-have, I know. But I didn't have much time to write proper tests for this app.