const request = require('request');
const config = require('../config');

module.exports = {
    auth: (req, res) => {
        const authOptions = {
            url: config.authUrl,
            headers: {
                'Authorization': 'Basic ' + (Buffer.from(config.clientId + ':' + config.clientSecret).toString('base64'))
            },
            form: {
                grant_type: 'client_credentials'
            },
            json: true
        };
        request.post(authOptions, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                res.status(200).json({
                    token: body.access_token
                })
            }
        });
    },
    getPlaylists: (req, res) => {
        const {authorization} = req.headers;
        const authOptions = {
            url: config.apiBaseUrl + '/browse/featured-playlists',
            headers: {
                'Authorization': authorization
            }
        };
        request.get(authOptions, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                res.status(200).send(body)
            }
        });
    },
    getPlaylistTracks: (req, res) => {
        const {authorization} = req.headers;
        const {id} = req.params
        const authOptions = {
            url: config.apiBaseUrl + `/playlists/${id}/tracks`,
            headers: {
                'Authorization': authorization
            }
        };
        request.get(authOptions, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                res.status(200).send(body)
            }
        });
    },
    getTrackDetails: (req, res) => {
        const {authorization} = req.headers;
        const {id} = req.params
        const authOptions = {
            url: config.apiBaseUrl + `/tracks/${id}`,
            headers: {
                'Authorization': authorization
            }
        };
        request.get(authOptions, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                res.status(200).send(body)
            }
        });
    }
}